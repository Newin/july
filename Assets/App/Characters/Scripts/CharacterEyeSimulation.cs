﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterEyeSimulation : MonoBehaviour
{

    public bool Simulate;
    private CharacterEye characterEye;
    public Vector2 TimeBeforeChangeDirection;
    private Vector2 eyeDirection;
    private float timer;
    private float randomTime;

    public void SimulationUpdate()
    {
        if (characterEye == null)
        {
            characterEye = this.gameObject.GetComponent<CharacterEye>();
        }

        if (Simulate == true)
        {
            


            timer +=Time.deltaTime;


            if (timer > randomTime)
            {
                eyeDirection = Random.insideUnitCircle;

                timer = 0;
                randomTime = Random.Range(TimeBeforeChangeDirection.x *1f, TimeBeforeChangeDirection.y * 1f);
            }

            characterEye.PupilTranslationX = Mathf.Lerp(characterEye.PupilTranslationX , eyeDirection.x,Time.deltaTime*25);
            characterEye.PupilTranslationY = Mathf.Lerp(characterEye.PupilTranslationY, eyeDirection.y*0.5f, Time.deltaTime * 25);
        }
        else
        {
            characterEye.PupilTranslationX = 0;
            characterEye.PupilTranslationY = 0;
        }


    }
}
