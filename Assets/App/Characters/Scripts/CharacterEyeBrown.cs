﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterEyeBrown : MonoBehaviour
{
    [Header("Customisation")]
    [Range(-1, 1)]
    public float EyeBrownDistance = 0;
    [Range(-1, 1)]
    public float EyeBrownHeight = 0;
    [Range(0, 1.15f)]
    public float EyeBrownSize = 1;
    public Color PupilColor = Color.grey;


    [Header("Textures")]
    public Texture2D EyeBrownTexture;


    [Header("Animation")]
    [Range(-1, 1)]
    public float EyeBrownTranslationY = 0;
    [Header("Form")]
    [Range(-1, 1)]
    public float EyeBrownLTranslationA = 0;
    [Range(-1, 1)]
    public float EyeBrownLTranslationB = 0;
    [Range(-1, 1)]
    public float EyeBrownLTranslationC = 0;

    [Range(-1, 1)]
    public float EyeBrownRTranslationC = 0;
    [Range(-1, 1)]
    public float EyeBrownRTranslationB = 0;
    [Range(-1, 1)]
    public float EyeBrownRTranslationA = 0;



    [Header("Material")]
    public List<Material> EyeBrownMaterials;


    public void VisualUpdate()
    {

        for (int i = 0; i < EyeBrownMaterials.Count; i++)
        {
            EyeBrownMaterials[i].SetVector("_EyeBrownTranslation", new Vector4(EyeBrownDistance * 0.15f, (-EyeBrownHeight * 0.15f) + (-EyeBrownTranslationY * 0.25f), 0, 0));

            EyeBrownMaterials[i].SetFloat("_EyeBrownSize", EyeBrownSize);

            EyeBrownMaterials[i].SetColor("_EyeBrownColor", PupilColor);

            EyeBrownMaterials[i].SetTexture("_EyeBrownTexture", EyeBrownTexture);
        }

        EyeBrownMaterials[0].SetVector("_EyeBrownForm", new Vector4(EyeBrownLTranslationA, EyeBrownLTranslationB, EyeBrownLTranslationC, 0));
        EyeBrownMaterials[1].SetVector("_EyeBrownForm", new Vector4(EyeBrownRTranslationA, EyeBrownRTranslationB, EyeBrownRTranslationC, 0));



    }
}
