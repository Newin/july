﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterSkin : MonoBehaviour
{
    [Header("Customisation")]
    public Color SkinColor = Color.grey;

    [Header("Material")]
    public List<Material> SkinMaterials;

    public void VisualUpdate()
    {

        for (int i = 0; i < SkinMaterials.Count; i++)
        {
            SkinMaterials[i].SetColor("_SkinColor", SkinColor);
        }

    }
}
