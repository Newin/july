﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class Mouth
{
    

    public enum Types
    {
        Open,
        Close,
        Special
    }
}

[CreateAssetMenu(menuName = "Mouth/MouthSheet")]
[System.Serializable]
public class MouthSheet : ScriptableObject
{

    [Header("Type")]
    static public string MouthName = "";
    public Mouth.Types mouthType;

    [Header("Visual")]
    public int MouthSpriteNumber = 0;
    public float TeethTopPosition = 0;
    public float TeethBotPosition = 0;
}
