﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterHair : MonoBehaviour
{
    [Header("Customisation")]
    public Color HairColor = Color.grey;
    public bool TwoColor = false;
    public Color HairTieColor = Color.grey;

    [Range(0,2)]
    public float TiePower = 1;

    [Range(-1,1)]
    public float TieHeight = 0;

    [Header("Material")]
    public Material HairMaterial;

    public void VisualUpdate()
    {

        HairMaterial.SetColor("_HairColor", HairColor);

        HairMaterial.SetColor("_HairColorTie", HairTieColor);

        if (TwoColor == false)
        {
            HairMaterial.SetColor("_HairColorTie", HairColor);
        }

        HairMaterial.SetVector("_HairTieEffect", new Vector4(TiePower, -TieHeight*0.5f, 0,0));


    }
}
