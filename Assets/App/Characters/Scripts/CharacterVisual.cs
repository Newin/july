﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterVisual : MonoBehaviour
{
    private CharacterEye characterEye;
    private CharacterEyeBrown characterEyeBrown;
    private CharacterMouth characterMouth;
    private CharacterSkin characterSkin;
    private CharacterHair characterHair;


    private CharacterEyeSimulation characterEyeSimulation;

    void Update()
    {
        if (characterEye == null)
        {
            characterEye = this.gameObject.GetComponent<CharacterEye>();
        }
        if (characterEyeBrown == null)
        {
            characterEyeBrown = this.gameObject.GetComponent<CharacterEyeBrown>();
        }
        if (characterMouth == null)
        {
            characterMouth = this.gameObject.GetComponent<CharacterMouth>();
        }
        if (characterSkin == null)
        {
            characterSkin = this.gameObject.GetComponent<CharacterSkin>();
        }
        if (characterHair == null)
        {
            characterHair = this.gameObject.GetComponent<CharacterHair>();
        }


        if (characterEyeSimulation == null)
        {
            characterEyeSimulation = this.gameObject.GetComponent<CharacterEyeSimulation>();
        }
        else
        {
            characterEyeSimulation.SimulationUpdate();
        }

        characterEye.VisualUpdate();
        characterEyeBrown.VisualUpdate();
        characterMouth.VisualUpdate();
        characterSkin.VisualUpdate();
        characterHair.VisualUpdate();


    }
}
