﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterEye : MonoBehaviour
{

    [Header("Customisation")]
    [Range(-1, 1)]
    public float EyeDistance = 0;
    [Range(-1, 1)]
    public float EyeHeight = 0;
    [Range(0, 1.2f)]
    public float EyeSize = 1;
    public Color PupilColor = Color.grey;


    [Header("Textures")]
    public Texture2D EyeTexture;
    public Texture2D PupilTexture;


    [Header("Animation")]
    [Range(-1, 1)]
    public float PupilSize = 0;
    [Range(-1, 1)]
    public float PupilTranslationX = 0;
    [Range(-1, 1)]
    public float PupilTranslationY = 0;


    [Header("Material")]
    public List<Material> EyeMaterials;


    // Update is called once per frame
    public void VisualUpdate()
    {

        for (int i = 0; i < EyeMaterials.Count; i++)
        {
            EyeMaterials[i].SetVector("_EyeTranslation", new Vector4(EyeDistance*0.05f, -EyeHeight*0.05f, 0, 0));

            EyeMaterials[i].SetFloat("_EyeGlobalScale", EyeSize);

            EyeMaterials[i].SetColor("_PupilColor", PupilColor);

            EyeMaterials[i].SetTexture("_EyeTexture", EyeTexture);

            EyeMaterials[i].SetTexture("_PupilTexture", PupilTexture);

            EyeMaterials[i].SetFloat("_PupilSize", PupilSize+1);

            EyeMaterials[i].SetVector("_PupilTranslation", new Vector4(PupilTranslationX, -PupilTranslationY, 0, 0));

        }

    }
}
