﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterMouth : MonoBehaviour
{


    [Header("Customisation")]
    [Range(-1, 1)]
    public float MouthHeight = 0;
    [Range(0, 2f)]
    public float MouthSize = 1;
    public Color LipColor = Color.red;

    public MouthSheet currentMouth;

    [Header("Textures")]
    public Texture2D MouthTexture;

    [Header("Material")]
    public Material MouthMaterial;


    public void VisualUpdate()
    {

        MouthMaterial.SetColor("_LipColor", LipColor);
        MouthMaterial.SetInt("_MouthSpriteNumber", currentMouth.MouthSpriteNumber);

        MouthMaterial.SetVector("_MouthTranslation", new Vector4(0, MouthHeight*0.25f, 0, 0));
        MouthMaterial.SetFloat("_MouthSize", MouthSize);
        MouthMaterial.SetTexture("_MouthLipSprite", MouthTexture);

        MouthMaterial.SetFloat("_TeethTopPosition", Mathf.Lerp(MouthMaterial.GetFloat("_TeethTopPosition"), currentMouth.TeethTopPosition,Time.deltaTime*50));
        MouthMaterial.SetFloat("_TeethBotPosition", Mathf.Lerp(MouthMaterial.GetFloat("_TeethBotPosition"), currentMouth.TeethBotPosition, Time.deltaTime*50));
    }
}
