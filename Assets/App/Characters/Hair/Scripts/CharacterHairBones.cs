﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class CharacterHairBones : MonoBehaviour
{

    private GameObject head;
    private GameObject parentBone;

    private Quaternion initialRotation;
    public Vector3 initialEulerRotation;

    public Vector4 BoneClampLimits;
    private float bonePower;
    private Vector3 lastBonePosition;
    public Vector3 BoneVelocity;
    public float BoneMagnitude;

    public void OnEnable()
    {
        initialRotation = this.transform.localRotation;
        initialEulerRotation = this.transform.localEulerAngles;
        lastBonePosition = this.transform.position;
        /*
        if (this.gameObject.GetComponent<CharacterHairBonesHandle>() == null)
        {
            this.gameObject.AddComponent<CharacterHairBonesHandle>();
        }
        */
    }

    public void BoneGenerate(Vector4 boneClampLimits, GameObject mainChainBone,float power)
    {
        BoneClampLimits = boneClampLimits;
        head = mainChainBone;
        bonePower = power;

        parentBone = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    public void BoneUpdate()
    {
        BoneVelocity =  lastBonePosition - this.transform.position;
        lastBonePosition = this.transform.position;



        Vector3 forward = new Vector3(head.transform.forward.x, Vector3.forward.y, head.transform.forward.z);
        Vector3 right = new Vector3(-head.transform.up.x, Vector3.forward.y, -head.transform.up.z);
        Vector3 back = new Vector3(-head.transform.forward.x, Vector3.forward.y, -head.transform.forward.z);
        Vector3 left = new Vector3(head.transform.up.x, Vector3.forward.y, head.transform.up.z);


        BoneMagnitude = BoneVelocity.magnitude *2;

        if (Vector3.Angle(BoneVelocity.normalized, forward) < 45)
        {
            //BoneMagnitude = BoneMagnitude * BoneClampLimits.x;
            BoneMagnitude = Mathf.Clamp(BoneMagnitude, 0, BoneClampLimits.x/10);
        }

        if (Vector3.Angle(BoneVelocity.normalized, right) < 45)
        {
            //BoneMagnitude = BoneMagnitude * BoneClampLimits.y;
            BoneMagnitude = Mathf.Clamp(BoneMagnitude, 0, BoneClampLimits.y/10);
        }

        if (Vector3.Angle(BoneVelocity.normalized, back) < 45)
        {
            //BoneMagnitude = BoneMagnitude * BoneClampLimits.z;
            BoneMagnitude = Mathf.Clamp(BoneMagnitude, 0, BoneClampLimits.z/10);
        }

        if (Vector3.Angle(BoneVelocity.normalized, left) < 45)
        {
            //BoneMagnitude = BoneMagnitude * BoneClampLimits.w;
            BoneMagnitude = Mathf.Clamp(BoneMagnitude, 0, BoneClampLimits.w/10);
        }



        //BoneVelocity = this.transform.InverseTransformDirection(BoneVelocity);
        //BoneVelocity = BoneVelocity * 20;

        //this.transform.Rotate(BoneVelocity.x, BoneVelocity.y, BoneVelocity.z, Space.World);



        this.transform.RotateAround(Vector3.Cross(BoneVelocity, Vector3.up).normalized, BoneMagnitude);

        //this.transform.RotateAround(Vector3.Cross(BoneVelocity, -Vector3.up).normalized, BoneVelocity.magnitude * 2f);

        this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, initialRotation, Time.deltaTime * 10);


        /*
        this.transform.localEulerAngles = new Vector3(
            this.transform.localEulerAngles.x,
            Mathf.Clamp(this.transform.localEulerAngles.y, BoneRotationLimits.y + initialEulerRotation.y, BoneRotationLimits.x + initialEulerRotation.y),
            Mathf.Clamp(this.transform.localEulerAngles.z, BoneRotationLimits.w + initialEulerRotation.z, BoneRotationLimits.z + initialEulerRotation.z));
        

        this.transform.localEulerAngles = new Vector3(
            this.transform.localEulerAngles.x,
            this.transform.localEulerAngles.y,
            Mathf.Clamp(this.transform.localEulerAngles.z, BoneRotationLimits.z + initialEulerRotation.z, BoneRotationLimits.w + initialEulerRotation.z));
       */

        /*
        if ( ((this.transform.localEulerAngles.y+180)%360)-180 < 0 )
        {
            this.transform.localEulerAngles = new Vector3(this.transform.localEulerAngles.x,  0 ,this.transform.localEulerAngles.z);
            Debug.Log("intrusion");
        } 
        */
    }

    
    void OnDrawGizmos()
    {
        /*
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(this.transform.position, -transform.right * 0.25f);

        Handles.color = new Color(0, 0, 1, 0.2f);
        Handles.DrawSolidArc(this.transform.position,  new Vector3(head.transform.forward.x, this.transform.forward.y, head.transform.forward.z), -parentBone.transform.right, -BoneRotationLimits.x , 0.05f);

        Handles.DrawSolidArc(this.transform.position, new Vector3(head.transform.forward.x, this.transform.forward.y, head.transform.forward.z), -parentBone.transform.right, -BoneRotationLimits.y, 0.05f);



        Handles.color = new Color(0, 1, 0, 0.2f);
        Handles.DrawSolidArc(this.transform.position, new Vector3(head.transform.up.x, this.transform.forward.y, head.transform.up.z), -parentBone.transform.right, -BoneRotationLimits.z, 0.05f);

        Handles.DrawSolidArc(this.transform.position, new Vector3(head.transform.up.x, this.transform.forward.y, head.transform.up.z), -parentBone.transform.right, -BoneRotationLimits.w, 0.05f);
        */
        /*
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(this.transform.position, new Vector3(head.transform.forward.x, Vector3.forward.y, head.transform.forward.z) * BoneClampLimits.x *0.05f);

        Gizmos.DrawRay(this.transform.position, new Vector3(-head.transform.up.x, Vector3.forward.y, -head.transform.up.z) * BoneClampLimits.y * 0.05f);

        Gizmos.DrawRay(this.transform.position, new Vector3(-head.transform.forward.x, Vector3.forward.y, -head.transform.forward.z) * BoneClampLimits.z * 0.05f);

        Gizmos.DrawRay(this.transform.position, new Vector3(head.transform.up.x, Vector3.forward.y, head.transform.up.z) * BoneClampLimits.w * 0.05f);

        

        

        


        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(this.transform.position, BoneVelocity.normalized * BoneMagnitude * 1);
        */
        //hand.draw

    }
    
}
