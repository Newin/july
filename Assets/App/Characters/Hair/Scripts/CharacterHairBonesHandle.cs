﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CharacterHairBonesHandle : MonoBehaviour
{

    private CharacterHairBones characterHairBones;

    void OnSceneGUI()
    {

        if (characterHairBones == null)
        {
            characterHairBones = this.gameObject.GetComponent<CharacterHairBones>();
        }


        Handles.color = new Color(0, 0, 1, 1f);
        Handles.DrawSolidArc(this.transform.position, this.transform.up, -this.transform.right,180,1);

    }
}
