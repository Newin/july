﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BonesClamp
{
    public float power = 2;
    public Vector4 maxVelocity = new Vector4(-20f,20f,-20f,20f);
}

[System.Serializable]
public class HairBones
{
    public List<BonesClamp> bonesClamp;
}

[CreateAssetMenu(menuName = "Hair/HairPhysicSheet")]
[System.Serializable]
public class HairPhysicSheet : ScriptableObject
{

    [Header("Bones")]
    public List<HairBones> HairBoneList;
}
