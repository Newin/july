﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHairPhysic : MonoBehaviour
{


    public HairPhysicSheet hairPhysicSheet;
    public GameObject mainChain;
    public List<CharacterHairBones> bonesChainList;



    // Start is called before the first frame update
    void Awake()
    {
        Generate();
    }

    private void Update()
    {
        for (int i = 0; i < bonesChainList.Count; i++)
        {
            bonesChainList[i].BoneUpdate();
        }
    }

    // Update is called once per frame
    void Generate()
    {
        for (int i = 0; i < hairPhysicSheet.HairBoneList.Count; i++)
        {
            for (int j = 0; j < hairPhysicSheet.HairBoneList[i].bonesClamp.Count; j++)
            {
                GameObject targetBones = null;
                if (j == 0)
                {
                    targetBones = mainChain.transform.GetChild(i).gameObject;
                }
                if (j == 1)
                {
                    targetBones = mainChain.transform.GetChild(i).GetChild(0).gameObject;
                }
                if (j == 2)
                {
                    targetBones = mainChain.transform.GetChild(i).GetChild(0).GetChild(0).gameObject;
                }

                if (targetBones.gameObject.GetComponent<CharacterHairBones>() == null)
                {
                    targetBones.gameObject.AddComponent<CharacterHairBones>();
                    bonesChainList.Add(targetBones.gameObject.GetComponent<CharacterHairBones>());
                    targetBones.gameObject.GetComponent<CharacterHairBones>().BoneGenerate(hairPhysicSheet.HairBoneList[i].bonesClamp[j].maxVelocity, mainChain, hairPhysicSheet.HairBoneList[i].bonesClamp[j].power);
                }

            }
        }
    }
}
